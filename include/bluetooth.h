#ifndef __BLUETOOTH_H__
#define __BLUETOOTH_H__

/*
  Bluetooth remote control mode
*/
void bluetooth_mode();

void get_bluetooth_data();

#endif // __BLUETOOTH_H__
