#pragma once

#include <WString.h>

/*Arduino pin is connected to the Motor drive module*/
#define ENA 5
#define ENB 6
#define IN1 7
#define IN2 8
#define IN3 9
#define IN4 11

#define MAX_CAR_SPEED UINT8_MAX // PWM (Motor speed/Speed)

// The coefficient applied to get the inner wheels to rotate slower when turning
#define TURN_COEFFICIENT 0.25

enum MOTIONMODE
{
  LEFT,    /*left*/
  RIGHT,   /*right*/
  FORWARD, /*forward*/
  BACK,    /*backward*/
  STOP,    /*stop*/
  FORWARD_LEFT,
  FORWARD_RIGHT,
  BACKWARD_LEFT,
  BACKWARD_RIGHT,
};

extern enum MOTIONMODE mov_mode;

extern uint8_t g_car_speed_rocker;

void move();

/*
  Control motor：Car movement forward
*/
void forward(uint8_t in_carSpeed);

/*
  Control motor：Car moving backwards
*/
void backward(uint8_t in_carSpeed);

/*
  Control motor：The car turns left and moves forward
*/
void left(uint8_t in_carSpeed);

/*
  Control motor：The car turns right and moves forward
*/
void right(uint8_t in_carSpeed);

/*
  Stop motor control：Turn off the motor drive
*/
void stop();

void forward_left(uint8_t in_carSpeed, double inside_wheels_reduction);

void forward_right(uint8_t in_carSpeed, double inside_wheels_reduction);

void back_left(uint8_t in_carSpeed, double inside_wheels_reduction);

void back_right(uint8_t in_carSpeed, double inside_wheels_reduction);
