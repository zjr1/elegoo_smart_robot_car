#include "motor.h"

#include "arduino_debug.h"
#include "utility.h"

#include <Arduino.h>

enum MOTIONMODE mov_mode = STOP;

uint8_t g_car_speed_rocker = MAX_CAR_SPEED;

void move()
{
    if (func_mode == IRremote || func_mode == Bluetooth)
    {
        switch (mov_mode)
        {
        case LEFT:
            left(g_car_speed_rocker);
            break;

        case RIGHT:
            right(g_car_speed_rocker);
            break;

        case FORWARD:
            forward(g_car_speed_rocker);
            break;

        case BACK:
            backward(g_car_speed_rocker);
            break;

        case STOP:
            stop();
            break;

        case FORWARD_LEFT:
            forward_left(g_car_speed_rocker, TURN_COEFFICIENT);
            break;

        case BACKWARD_LEFT:
            back_left(g_car_speed_rocker, TURN_COEFFICIENT);
            break;

        case FORWARD_RIGHT:
            forward_right(g_car_speed_rocker, TURN_COEFFICIENT);
            break;

        case BACKWARD_RIGHT:
            back_right(g_car_speed_rocker, TURN_COEFFICIENT);
            break;

        default:
            break;
        }
    }
}

void forward(uint8_t in_carSpeed)
{
    analogWrite(ENA, in_carSpeed);
    analogWrite(ENB, in_carSpeed);
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);

    // debug_serial_println("forward");
}

void backward(uint8_t in_carSpeed)
{
    analogWrite(ENA, in_carSpeed);
    analogWrite(ENB, in_carSpeed);
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);

    // debug_serial_println("backward");
}

void left(uint8_t in_carSpeed)
{

    analogWrite(ENA, in_carSpeed);
    analogWrite(ENB, in_carSpeed);
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);

    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);

    // debug_serial_println("left");
}

void right(uint8_t in_carSpeed)
{
    analogWrite(ENA, in_carSpeed);
    analogWrite(ENB, in_carSpeed);
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);

    // debug_serial_println("right");
}

void stop()
{
    digitalWrite(ENA, LOW);
    digitalWrite(ENB, LOW);

    // debug_serial_println("stop");
}

void forward_left(uint8_t in_carSpeed, double inside_wheels_reduction)
{
    analogWrite(ENA, in_carSpeed * inside_wheels_reduction);
    analogWrite(ENB, in_carSpeed);
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);

    // debug_serial_println("forward left");
}

void forward_right(uint8_t in_carSpeed, double inside_wheels_reduction)
{
    analogWrite(ENA, in_carSpeed);
    analogWrite(ENB, in_carSpeed * inside_wheels_reduction);
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);

    // debug_serial_println("forward right");
}

void back_left(uint8_t in_carSpeed, double inside_wheels_reduction)
{
    analogWrite(ENA, in_carSpeed * inside_wheels_reduction);
    analogWrite(ENB, in_carSpeed);
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);

    // debug_serial_println("backward left");
}

void back_right(uint8_t in_carSpeed, double inside_wheels_reduction)
{
    analogWrite(ENA, in_carSpeed);
    analogWrite(ENB, in_carSpeed * inside_wheels_reduction);
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);

    // debug_serial_println("backward right");
}
