#include "bluetooth.h"

#include "arduino_debug.h"
#include "motor.h"
#include "utility.h"

#include <Arduino.h>

void bluetooth_mode()
{
    if (func_mode == Bluetooth)
    {
        switch (mov_mode)
        {
        case LEFT:
            left(g_car_speed_rocker);
            break;
        case RIGHT:
            right(g_car_speed_rocker);
            break;
        case FORWARD:
            forward(g_car_speed_rocker);
            break;
        case BACK:
            backward(g_car_speed_rocker);
            break;
        case STOP:
            stop();
            break;
        case FORWARD_LEFT:
            forward_left(g_car_speed_rocker, TURN_COEFFICIENT);
            break;
        case BACKWARD_LEFT:
            back_left(g_car_speed_rocker, TURN_COEFFICIENT);
            break;
        case FORWARD_RIGHT:
            forward_right(g_car_speed_rocker, TURN_COEFFICIENT);
            break;
        case BACKWARD_RIGHT:
            back_right(g_car_speed_rocker, TURN_COEFFICIENT);
            break;
        default:
            break;
        }
    }
}

void get_bluetooth_data()
{
    if (Serial.available() > 0)
    {
        char command = Serial.read();
        debug_serial_println("HERE");
        debug_serial_println(command);
        switch (command)
        {
        case 'F':
            func_mode = Bluetooth;
            mov_mode = FORWARD;
            break;

        case 'B':
            func_mode = Bluetooth;
            mov_mode = BACK;
            break;

        case 'L':
            func_mode = Bluetooth;
            mov_mode = LEFT;
            break;

        case 'R':
            func_mode = Bluetooth;
            mov_mode = RIGHT;
            break;

        case 'I':
            func_mode = Bluetooth;
            mov_mode = FORWARD_RIGHT;
            break;

        case 'G':
            func_mode = Bluetooth;
            mov_mode = FORWARD_LEFT;
            break;

        case 'J':
            func_mode = Bluetooth;
            mov_mode = BACKWARD_RIGHT;
            break;

        case 'H':
            func_mode = Bluetooth;
            mov_mode = BACKWARD_LEFT;
            break;

        case 'S':
        case 'x': // From button for turning off hazard sign
        case 'w': // From button for turning off front lights
            func_mode = Bluetooth;
            mov_mode = STOP;
            break;

        case 'X': // From button for turning on hazard sign
            func_mode = LineTracking;
            break;

        case 'W': // From button for turning on front lights
            func_mode = ObstaclesAvoidance;
            break;

        default:
            break;
        }
    }
}
